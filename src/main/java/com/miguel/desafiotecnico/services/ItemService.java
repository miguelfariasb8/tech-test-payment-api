package com.miguel.desafiotecnico.services;

import com.miguel.desafiotecnico.mapper.ItemMapper;
import com.miguel.desafiotecnico.models.Item;
import com.miguel.desafiotecnico.models.dto.ItemDTO;
import com.miguel.desafiotecnico.repository.ItemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ItemService {

    private final ItemRepository itemRepository;

    public Item save(ItemDTO itemDTO) {

        Item item = ItemMapper.toItem(itemDTO);

        log.info(String.format("Saved %s in database", itemDTO.getName()));

        return itemRepository.save(item);
    }
}
