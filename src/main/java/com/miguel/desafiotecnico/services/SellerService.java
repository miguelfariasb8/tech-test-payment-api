package com.miguel.desafiotecnico.services;

import com.miguel.desafiotecnico.mapper.SellerMapper;
import com.miguel.desafiotecnico.models.Seller;
import com.miguel.desafiotecnico.models.dto.SellerDTO;
import com.miguel.desafiotecnico.repository.SellerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SellerService {

    private final SellerRepository sellerRepository;

    public Seller save(SellerDTO sellerDTO) {

        Seller seller = SellerMapper.toSeller(sellerDTO);

        log.info(String.format("Saved %s seller in database", sellerDTO.getName()));

        return sellerRepository.save(seller);
    }
}
