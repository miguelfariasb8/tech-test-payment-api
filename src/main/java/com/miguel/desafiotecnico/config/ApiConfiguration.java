package com.miguel.desafiotecnico.config;

import com.miguel.desafiotecnico.models.Item;
import com.miguel.desafiotecnico.models.Sale;
import com.miguel.desafiotecnico.models.Seller;
import com.miguel.desafiotecnico.repository.ItemRepository;
import com.miguel.desafiotecnico.repository.SaleRepository;
import com.miguel.desafiotecnico.repository.SellerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.List;


@Configuration
@RequiredArgsConstructor
public class ApiConfiguration implements CommandLineRunner {

    private final SellerRepository sellerRepository;
    private final SaleRepository saleRepository;
    private final ItemRepository itemRepository;


    @Override
    public void run(String... args) throws Exception {

        Item item = new Item(1L,"Notebook Gamer", BigDecimal.valueOf(3500));

        Seller seller = new Seller(1L, "Miguel", "Miguel@test.com",
                "284.094.700-55", "123456789");

        Sale sale = new Sale(1L, 1L, List.of(item));

        sellerRepository.save(seller);

        itemRepository.save(item);

        saleRepository.save(sale);
    }
}
