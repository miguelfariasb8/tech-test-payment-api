package com.miguel.desafiotecnico.mapper;

import com.miguel.desafiotecnico.models.Seller;
import com.miguel.desafiotecnico.models.dto.SellerDTO;
import org.springframework.stereotype.Component;

@Component
public class SellerMapper {

    public static Seller toSeller(SellerDTO sellerDTO) {

        return new Seller(sellerDTO);
    }
}
