package com.miguel.desafiotecnico.mapper;

import com.miguel.desafiotecnico.models.Item;
import com.miguel.desafiotecnico.models.dto.ItemDTO;
import org.springframework.stereotype.Component;

@Component
public class ItemMapper {

    public static Item toItem(ItemDTO itemDTO) {

        return new Item(itemDTO.getName(), itemDTO.getPrice());
    }
}
