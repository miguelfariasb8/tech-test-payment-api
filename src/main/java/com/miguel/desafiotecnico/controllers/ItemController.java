package com.miguel.desafiotecnico.controllers;

import com.miguel.desafiotecnico.models.Item;
import com.miguel.desafiotecnico.models.dto.ItemDTO;
import com.miguel.desafiotecnico.models.response.ItemResponse;
import com.miguel.desafiotecnico.services.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/api/v1/item")
@RequiredArgsConstructor
public class ItemController {

    private final ItemService itemService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ResponseEntity<ItemResponse> save(ItemDTO itemDTO) {

        Item item = itemService.save(itemDTO);

        ItemResponse itemResponse = new ItemResponse(item);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(item.getId()).toUri();

        return ResponseEntity.created(uri).body(itemResponse);
    }
}
