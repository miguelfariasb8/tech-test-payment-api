package com.miguel.desafiotecnico.controllers;

import com.miguel.desafiotecnico.models.Sale;
import com.miguel.desafiotecnico.models.Seller;
import com.miguel.desafiotecnico.repository.ItemRepository;
import com.miguel.desafiotecnico.repository.SaleRepository;
import com.miguel.desafiotecnico.repository.SellerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("/api/v1/sale")
@RestController
@RequiredArgsConstructor
public class SaleController {

    private final SellerRepository sallerRepository;
    private final SaleRepository saleRepository;
    private final ItemRepository itemRepository;

    @PostMapping
    public ResponseEntity<Sale> create(@RequestBody Sale sale) {

        Long sallerId = sale.getSellerId();

        Optional<Seller> saller = sallerRepository.findById(sallerId);

        sallerRepository.save(saller.get());

        saleRepository.save(sale);

        itemRepository.saveAll(sale.getItems());

        //URI uri = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(seller.getId()).toUri();

        return ResponseEntity.status(201).body(sale);
    }
}
