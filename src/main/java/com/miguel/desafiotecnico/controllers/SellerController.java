package com.miguel.desafiotecnico.controllers;

import com.miguel.desafiotecnico.models.Seller;
import com.miguel.desafiotecnico.models.dto.SellerDTO;
import com.miguel.desafiotecnico.models.response.SellerResponse;
import com.miguel.desafiotecnico.services.SellerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/api/v1/seller")
@RequiredArgsConstructor
public class SellerController {

    private final SellerService sellerService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<SellerResponse> create(@RequestBody SellerDTO sellerDTO) {

        Seller seller = sellerService.save(sellerDTO);

        SellerResponse sellerResponse = new SellerResponse(seller);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(seller.getId()).toUri();

        return ResponseEntity.created(uri).body(sellerResponse);
    }
}
