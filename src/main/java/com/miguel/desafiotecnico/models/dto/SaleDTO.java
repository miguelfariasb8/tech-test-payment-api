package com.miguel.desafiotecnico.models.dto;

import com.miguel.desafiotecnico.models.Item;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SaleDTO implements Serializable {

    @Positive
    private Long sellerId;
    private LocalDate date = LocalDate.now();
    private List<Item> items = new ArrayList<>();
}
