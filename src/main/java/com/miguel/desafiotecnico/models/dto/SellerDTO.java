package com.miguel.desafiotecnico.models.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.br.CPF;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SellerDTO implements Serializable {

    @NotNull
    @NotEmpty
    @NotBlank
    private String name;

    @Email
    private String email;

    @CPF
    private String cpf;

    @NotNull
    @NotEmpty
    @NotBlank
    private String phone;
}
