package com.miguel.desafiotecnico.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sales")
public class Sale implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long sellerId;

    private LocalDate date = LocalDate.now();

    private Status status = Status.WAITING_PAYMENT;

    @OneToMany
    private List<Item> items;

    public Sale(Long id, Long sellerId, List<Item> items) {
        this.id = id;
        this.sellerId = sellerId;
        this.items = items;
    }
}
