package com.miguel.desafiotecnico.models;

import com.miguel.desafiotecnico.models.dto.SellerDTO;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sellers")
public class Seller implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String email;

    private String cpf;

    private String phone;

    public Seller(SellerDTO sellerDTO) {
        this.name = sellerDTO.getName();
        this.email = sellerDTO.getEmail();
        this.cpf = sellerDTO.getCpf();
        this.phone = sellerDTO.getPhone();
    }
}
