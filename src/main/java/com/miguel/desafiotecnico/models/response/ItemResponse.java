package com.miguel.desafiotecnico.models.response;

import com.miguel.desafiotecnico.models.Item;

import java.io.Serializable;
import java.math.BigDecimal;

public record ItemResponse(Long id, String name, BigDecimal price) implements Serializable {

    public ItemResponse(Item item) {

        this(item.getId(), item.getName(), item.getPrice());
    }
}
