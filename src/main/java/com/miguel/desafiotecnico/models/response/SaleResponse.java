package com.miguel.desafiotecnico.models.response;

import com.miguel.desafiotecnico.models.Item;
import com.miguel.desafiotecnico.models.Status;

import java.time.LocalDate;
import java.util.List;

public record SaleResponse(Long id, Long sellerId,
                           LocalDate date, Status status, List<Item> items) {
}
