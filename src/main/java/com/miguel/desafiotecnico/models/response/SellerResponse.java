package com.miguel.desafiotecnico.models.response;

import com.miguel.desafiotecnico.models.Seller;

public record SellerResponse(Long id, String name) {

    public SellerResponse(Seller seller) {
        this(seller.getId(), seller.getName());
    }
}
