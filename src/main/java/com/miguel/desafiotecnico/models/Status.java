package com.miguel.desafiotecnico.models;

public enum Status {

    WAITING_PAYMENT {

        @Override
        public String toString() {
            return "WAITING PAYMENT";
        }
    },
    CANCELED,
    PAYMENT_CONFIRMED {

        @Override
        public String toString() {
            return "PAYMENT CONFIRMED";
        }
    },

    SENT_TO_CARRIER {

        @Override
        public String toString() {
            return "SENT TO CARRIER";
        }
    },
    DELIVERED

}
