package com.miguel.desafiotecnico.states;

import com.miguel.desafiotecnico.models.Sale;
import com.miguel.desafiotecnico.models.Status;

public class WaitingPaymentState implements StatusSaleState{

    @Override
    public void updateSaleStatus(Sale sale, Status newStatus) {
        if(newStatus.equals(Status.PAYMENT_CONFIRMED) || newStatus.equals(Status.CANCELED)) {
            sale.setStatus(newStatus);
        }

        else
            throw new IllegalArgumentException(String.format
                    ("Cannot change from %s to %s", sale.getStatus(), newStatus));
    }
}
