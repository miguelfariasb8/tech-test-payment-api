package com.miguel.desafiotecnico.states;

import com.miguel.desafiotecnico.models.Sale;
import com.miguel.desafiotecnico.models.Status;

public interface StatusSaleState {
    void updateSaleStatus(Sale sale, Status newStatus);
}
