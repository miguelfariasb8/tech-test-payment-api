package com.miguel.desafiotecnico.repository;

import com.miguel.desafiotecnico.models.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerRepository extends JpaRepository<Seller, Long> {

    Seller findSellerByNameEqualsIgnoreCaseAndCpfEquals(String name, String cpf);
}
